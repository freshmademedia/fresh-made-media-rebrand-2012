/*
 * FMM Runtime jQuery
 * site-main.js
*/
//Runs on Window Load
$(window).load(function() {
// $(document).ready(function() {
   var btOriginal = 0
   var lgOriginal = 0;
   var sgOriginal = 0;

   //Top Image Animation
   btOriginal = $('.animate-image .bt').position().left;

   // Animation complete.
   lgOriginal = $('.animate-image .lg').position().left;

   // Animation complete.
   sgOriginal = $('.animate-image .sg').position().left;


   var lascrollPosScroll = 0;
   $(document).on('scroll', function() {

      //Small Circle
      var sgPosition  = $('.animate-image .sg').position().left;
      var sgPositionD = (sgPosition+2);
      var sgPositionU = (sgPosition-2);

      //Large Circle
      var lgPosition  = $('.animate-image .lg').position().left;
      var lgPositionD = (lgPosition+1);
      var lgPositionU = (lgPosition-1);

      //
      var btPosition  = $('.animate-image .bt').position().left;
      var btPositionD = (btPosition-1);
      var btPositionU = (btPosition+1);


      //Scroll Direction
      var scrollPos = $(this).scrollTop();
      if (scrollPos > lascrollPosScroll) {
         //Scroll Down
         $('.animate-image .bt').css("left", btPositionD+"px");
         $('.animate-image .sg').css("left", sgPositionD+"px");
         $('.animate-image .lg').css("left", lgPositionD+"px");
      } else {
         //Scroll Up
         $('.animate-image .bt').css("left", btPositionU+"px");
         $('.animate-image .sg').css("left", sgPositionU+"px");
         $('.animate-image .lg').css("left", lgPositionU+"px");

         if (scrollPos<10) {
            $('.animate-image .bt').animate({left: btOriginal+"px"});
            $('.animate-image .sg').animate({left: sgOriginal+"px"});
            $('.animate-image .lg').animate({left: lgOriginal+"px"});
         }
      }
      lascrollPosScroll=scrollPos;

   });

}); //runtime close